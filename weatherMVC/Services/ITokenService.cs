﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace weatherMVC.Services
{
    public interface ITokenService
    {
        Task<TokenResponse> GetToken(string scope);
    }
}